var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

// API Access link for creating client ID and secret:
// https://code.google.com/apis/console/
var GOOGLE_CLIENT_ID     = process.env.GOOGLE_CLIENT_ID;
var GOOGLE_CLIENT_SECRET = process.env.GOOGLE_CLIENT_SECRET;
var CALLBACK_URL = process.env.HTTP_HOST_AND_PORT + process.env.GOOGLE_CALLBACK_URL;

module.exports = function(passport, users) {

    passport.serializeUser(function(user, done) {
      //console.log(user);
      //req.session.user_email = user.emails[0].value;
      //req.session.save();
      done(null, user);
    });

    passport.deserializeUser(function(obj, done) {
      //console.log('deserializeUser');
      //console.log('obj', obj);
      done(null, obj);
    });

    // Use the GoogleStrategy within Passport.
    //   Strategies in Passport require a `verify` function, which accept
    //   credentials (in this case, an accessToken, refreshToken, and Google
    //   profile), and invoke a callback with a user object.
    passport.use(new GoogleStrategy({
        clientID:     GOOGLE_CLIENT_ID,
        clientSecret: GOOGLE_CLIENT_SECRET,
        callbackURL:  CALLBACK_URL
      },
      function(accessToken, refreshToken, profile, done) {
        // asynchronous verification, for effect...
        process.nextTick(function () {
          var email = profile.emails[0].value;

          //var users_emails = users.map(function(element){return element.email});
          var find_user_by_email = users.filter(function(v) { return v.email === email })[0];
          
          return done(null, find_user_by_email || false);
        });
      }
    ));
};