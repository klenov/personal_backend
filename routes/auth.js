
module.exports = 
function(options)
{
  var express  = require('express');
  var router   = express.Router();
  var passport = options.passport;
  var logger   = options.logger; 

  // GET /auth/google
  //   Use passport.authenticate() as route middleware to authenticate the
  //   request.  The first step in Google authentication will involve
  //   redirecting the user to google.com.  After authorization, Google
  //   will redirect the user back to this application at /auth/google/callback
  router.get('/auth/google',
    passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/userinfo.email'] }),
    function(req, res){
      // The request will be redirected to Google for authentication, so this
      // function will not be called.
  });

  // GET /auth/google/callback
  //   Use passport.authenticate() as route middleware to authenticate the
  //   request.  If authentication fails, the user will be redirected back to the
  //   login page.  Otherwise, the primary route function function will be called,
  //   which, in this example, will redirect the user to the home page.
  router.get(process.env.GOOGLE_CALLBACK_URL, 
    passport.authenticate('google', { failureRedirect: '/' }),
    function(req, res) {
      res.redirect('/upload');
  });

  router.get('/logout', function(req, res){
    req.logout();
    res.redirect('/');
  });

  return router;
}

