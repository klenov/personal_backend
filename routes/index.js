
module.exports = 
function(options)
{
  var logger   = options.logger;
  var app_path = options.app_path;
  var express = require('express');
  var router  = express.Router();
  var fs   = require('fs'); 
  var path = require('path');
  var util = require('util');

  function getFilesFolder(){
    return path.join(app_path, process.env.UPLOAD_FOLDER);
  }

  function fileUrl(file_name, folder){
    return process.env.UPLOAD_FOLDER_URL_PREFIX + util.format('%s/', folder) + file_name;
  }

  router.get('/', function(req, res) {
    //console.log('user', req.user);
    //console.log('USERS', JSON.parse(process.env.USERS) );
    if (req.isAuthenticated()) res.redirect('/upload');
    res.render('index', { title: 'Express', user: req.user });
  });


  // >> UPLOAD ROUTES
  var multer      = require('multer');
  var main_multer = multer({ 
    dest: process.env.UPLOAD_FOLDER,
    // rename: function (fieldname, filename) {
    //   console.log('fieldname', fieldname);
    //   console.log('filename', filename);
    //   //return Date.now() + '_' + filename.replace(/\W+/g, '-').toLowerCase();
    //   return filename.replace(/\W+/g, '-').toLowerCase();
    // }
    onFileUploadComplete: function (file)
    { 
      logger.info('onFileUploadComplete: ', file);
      if (/^[a-zA-Z0-9-_.]+$/.test(file.fieldname))
      {
        // записать в лог с какого айпи загружен файл?
        var tmp_files_folder     = getFilesFolder();
        var tmp_file_path        = path.join(tmp_files_folder, file.name);
        var user_specific_folder = path.join(tmp_files_folder, file.fieldname);
        var user_specific_file_path   = path.join(user_specific_folder, file.originalname)

        logger.info(tmp_file_path, '--->', user_specific_file_path);
        fs.mkdir(user_specific_folder, function(err)
        {
          if ( err.code != 'EEXIST' ) throw err;
          logger.info('file rename');
          fs.rename(tmp_file_path, user_specific_file_path, function (err) { if (err) throw err;})
        });
      } else
      {
        logger.error('Bad user login:', file.fieldname);
      }
      
    }
  });

  router.route('/upload')
    .get(isLoggedIn, function(req, res){

      var user_specific_folder = path.join(getFilesFolder(), req.user.folder);

      fs.readdir(user_specific_folder, function(err, files){
        if (err) throw err;
        var uploaded_files = files.map( function(e)
        {
          var file_url = fileUrl(e, req.user.folder);
          var res = e.match(/\.\w+$/);
          if(res)
            ext = res[0];
          else
            ext = '';

          return {name: e, size: 0, file_url: file_url, file_ext: ext}
        });
        //var json_uploaded_files = JSON.stringify( uploaded_files );
        res.render('upload', { title: 'Upload', user_folder: req.user.folder, user: req.user, uploaded_files: uploaded_files });
      });

    })
    .post(isLoggedIn, main_multer, function(req, res){
      logger.info(req.files);

      var first_key = Object.keys(req.files);
      var file_name = req.files[first_key].originalname;
      var file_url  = fileUrl(file_name, req.user.folder);
      res.json({ file_name: file_name, file_url: file_url, user: req.user })
    });
  // << UPLOAD ROUTES

  function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
      return next();

    res.redirect('/');
  }

  return router;
}
