'use strict';

var dotenv = require('dotenv');
dotenv.load();
var USERS = JSON.parse(process.env.USERS);

var fs = require('fs');

var express = require('express');
var path    = require('path');
var favicon = require('serve-favicon');
var access_logger  = require('morgan');
var bodyParser   = require('body-parser');

var winston = require('winston');
var logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({'timestamp':true}),
    new (winston.transports.File)({filename: path.join(__dirname, 'logs/app.log')})
  ]
});

var cookieParser   = require('cookie-parser');
var cookieSession  = require('cookie-session');
var passport       = require('passport');
var config_passport = require('./config/passport');
require('./config/passport')(passport, USERS);

var app = express();

var routes      = require('./routes/index')({ app_path: __dirname, logger: logger });
var auth_routes = require('./routes/auth')({  app_path: __dirname, logger: logger, passport: passport });

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
var winstonStream = {
  write: function(message, encoding){
      winston.info(message);
  }
};
app.use(access_logger('dev', {stream: winstonStream}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cookieSession({
  secret: process.env.SESSION_SECRET
}));
app.use(passport.initialize());
app.use(passport.session());

if (app.get('env') === 'development') {
  app.use(express.static(path.join(__dirname, 'public')));
  app.use('/bower_components', express.static(path.join(__dirname, 'bower_components')));
}

app.use('/', routes);
app.use('/', auth_routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
